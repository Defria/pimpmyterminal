#!/bin/bash

# https://www.programmerall.com/article/75232273331/

# if [ "$EUID" -ne 0 ]
#   then echo "Please run as root"
#   return
# fi

if [ -f "/etc/debian_version" ]
then
    sudo apt install git -y
    sudo apt install bind9-utils -y
    sudo bash -c 'for i in update {,full-,dist-}upgrade auto{remove,clean}; do apt-get $i -y; done'
else
    sudo yum update -y
    sudo yum install git -y
    sudo yum install bind9-utils -y
fi

sudo wget https://github.com/JanDeDobbeleer/oh-my-posh/releases/latest/download/posh-linux-amd64 -O /usr/local/bin/oh-my-posh --no-check-certificate
sudo chmod +x /usr/local/bin/oh-my-posh
mkdir ~/.poshthemes
sudo wget https://github.com/JanDeDobbeleer/oh-my-posh/releases/latest/download/themes.zip -O ~/.poshthemes/themes.zip --no-check-certificate
unzip -o  ~/.poshthemes/themes.zip -d ~/.poshthemes
sudo chmod u+rw ~/.poshthemes/*.json
rm -rf ~/.poshthemes/themes.zip


# Install nerd font
sudo wget https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/Meslo.zip -O ~/Meslo.zip --no-check-certificate
unzip -o ~/Meslo.zip -d Meslo
cd Meslo
sudo mkdir /usr/share/fonts/ttf
sudo cp *.ttf /usr/share/fonts/ttf
cd /usr/share/fonts/ttf
sudo chmod 744 *
sudo mkfontscale
sudo mkfontdir
sudo fc-cache -f -v

# Update profile scripts
echo 'eval "$(oh-my-posh --init --shell bash --config ~/.poshthemes/jandedobbeleer.omp.json)"' >> ~/.zshrc
echo 'eval "$(oh-my-posh --init --shell bash --config ~/.poshthemes/jandedobbeleer.omp.json)"' >> ~/.bashrc

# Install Z
# https://askubuntu.com/questions/250012/how-do-i-install-z-script
sudo wget https://raw.githubusercontent.com/rupa/z/master/z.sh -O ~/z.sh --no-check-certificate
echo . ~/z.sh >> ~/.bashrc
echo . ~/z.sh >> ~/.zshrc

# Customize vim
cd ~ 
touch .vimrc
echo 'set number' >> .vimrc
echo 'colorscheme desert' >> .vimrc

eval "$(oh-my-posh --init --shell bash --config ~/.poshthemes/jandedobbeleer.omp.json)"
cd ~
source .bashrc

sudo apt update && sudo apt dist-upgrade -y
sudo apt install build-essential curl file git -y
sudo apt install zsh -y
sudo apt install git-core curl fonts-powerline -y
sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
