# Install z https://askubuntu.com/questions/250012/how-do-i-install-z-script

sudo apt update && sudo apt upgrade -y
sudo apt install zsh -y
echo "Y" | sh -c "$(wget --no-check-certificate https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh -O -)"

# Customize Oh My Zsh with Syntax Highlighting and Auto-Suggestions
git clone https://github.com/zsh-users/zsh-autosuggestions.git $ZSH_CUSTOM/plugins/zsh-autosuggestions
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git $ZSH_CUSTOM/plugins/zsh-syntax-highlighting

vim ~/.zshrc

# Update your zsh list of plugins
plugins=(git zsh-autosuggestions zsh-syntax-highlighting)

# Make zsh default shell
chsh -s $(which zsh)
