# https://ohmyposh.dev/docs/installation/customize
# When using oh-my-posh in Windows and the WSL, know that you can share your theme with the WSL by pointing to a theme in your Windows user's home folder.
# Inside the WSL, you can find your Windows user's home folder here: /mnt/c/Users/<WINDOWSUSERNAME>.
export SHAREDHOME="/mnt/c/Users/$(whoami)/"
export POSHTHEMES="$SHAREDHOME/AppData/Local/Programs/oh-my-posh/themes"
eval "$(oh-my-posh --init --shell bash --config $POSHTHEMES/lambdageneration.omp.json)"
alias d-aws='eval "$(oh-my-posh --init --shell bash --config $POSHTHEMES/tokyo.omp.json)"'
alias d-windows='eval "$(oh-my-posh --init --shell bash --config $POSHTHEMES/montys.omp.json)"'
alias d-linux='eval "$(oh-my-posh --init --shell bash --config $POSHTHEMES/lambdageneration.omp.json)"'
